import {render, screen, act, fireEvent} from '@testing-library/react';
import Calculator, {isAllowedKey} from "../Calculator";

const base_test_id = 'calc_btn';
const display_id = 'calc_display';

describe('Calculator - Base Tests', () => {
  it('test display starts with "0"', () => {
    render(<Calculator/>);

    const display = screen.getByTestId(display_id);

    expect(display.textContent).toEqual('0');
  })
});

describe('Calculator - Button Tests', () => {
  const buttonList = [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' ];

  buttonList.forEach( (btn) => {
    it(`test "${btn}" button click`, () => {
      render(<Calculator/>);

      const button = screen.getByTestId(`${base_test_id}_${btn}`);
      const display = screen.getByTestId(display_id);

      act(() => button.click());
      expect(display.textContent).toEqual(btn);
    });

    it(`test "${btn}" key press`, () => {
      render(<Calculator/>);

      const calculator = screen.getByTestId('calc');
      const display = screen.getByTestId(display_id);

      // Use https://keycode.info to find keypress info
      const digitKeyCode = 48 + Number(btn);
      const numKeyCode = 96 + Number(btn);

      fireEvent.keyPress(calculator, {key: btn, code: `Digit${btn}`, charCode: digitKeyCode});
      fireEvent.keyPress(calculator, {key: btn, code: `Numpad${btn}`, charCode: numKeyCode});


      const expected = (btn === '0') ?
          '0' :
          `${btn}${btn}`;

      expect(display.textContent).toEqual(expected);
    });
  });

  it('test only allowed key presses', () => {
    render(<Calculator/>);

    const calculator = screen.getByTestId('calc');

    const testKeyCode = (num : number) => {
      if ( !isAllowedKey(String(num)) ) {
        // Key isn't allowed; make sure it cannot trigger anything
        fireEvent.keyPress(calculator, {charCode: num});
      }
    }

    for (let i = 58; i <= 226; i++) {
      testKeyCode(i);
    }

    // If we made it this far, no exceptions were thrown.
  });

  it('test "+" key press', () => {
    render(<Calculator/>);

    const calculator = screen.getByTestId('calc');
    const equal_button = screen.getByTestId(`${base_test_id}_=`);
    const display = screen.getByTestId(display_id);

    // Use https://keycode.info to find keypress info
    fireEvent.keyPress(calculator, {key: '1', code: 'Digit1', charCode: 49});
    fireEvent.keyPress(calculator, {key: '+', code: 'NumpadAdd', charCode: 107});
    fireEvent.keyPress(calculator, {key: '1', code: 'Digit1', charCode: 49});
    act(() => equal_button.click());
    fireEvent.keyPress(calculator, {key: '+', code: 'Equal', shiftKey: true, charCode: 187});
    fireEvent.keyPress(calculator, {key: '1', code: 'Digit1', charCode: 49});
    act(() => equal_button.click());

    expect(display.textContent).toEqual('3');
  });

  it('test "-" key press', () => {
    render(<Calculator/>);

    const calculator = screen.getByTestId('calc');
    const equal_button = screen.getByTestId(`${base_test_id}_=`);
    const display = screen.getByTestId(display_id);

    // Use https://keycode.info to find keypress info
    fireEvent.keyPress(calculator, {key: '6', code: 'Digit6', charCode: 54});
    fireEvent.keyPress(calculator, {key: '-', code: 'NumpadSubtract', charCode: 109});
    fireEvent.keyPress(calculator, {key: '1', code: 'Digit1', charCode: 49});
    act(() => equal_button.click());
    fireEvent.keyPress(calculator, {key: '-', code: 'Minus', shiftKey: false, charCode: 189});
    fireEvent.keyPress(calculator, {key: '1', code: 'Digit1', charCode: 49});
    act(() => equal_button.click());

    expect(display.textContent).toEqual('4');
  });

  it('test "÷" key press', () => {
    render(<Calculator/>);

    const calculator = screen.getByTestId('calc');
    const equal_button = screen.getByTestId(`${base_test_id}_=`);
    const display = screen.getByTestId(display_id);

    // Use https://keycode.info to find keypress info
    fireEvent.keyPress(calculator, {key: '1', code: 'Digit1', charCode: 49});
    fireEvent.keyPress(calculator, {key: '2', code: 'Digit2', charCode: 50});
    fireEvent.keyPress(calculator, {key: '/', code: 'NumpadDivide', charCode: 111});
    fireEvent.keyPress(calculator, {key: '2', code: 'Digit2', charCode: 50});
    act(() => equal_button.click());
    fireEvent.keyPress(calculator, {key: '/', code: 'Slash', shiftKey: false, charCode: 191});
    fireEvent.keyPress(calculator, {key: '2', code: 'Digit2', charCode: 50});
    act(() => equal_button.click());
    fireEvent.keyPress(calculator, {key: '\\', code: 'BackSlash', shiftKey: false, charCode: 220});
    fireEvent.keyPress(calculator, {key: '3', code: 'Digit3', charCode: 51});
    act(() => equal_button.click());

    expect(display.textContent).toEqual('1');
  });

  it('test "x" key press', () => {
    render(<Calculator/>);

    const calculator = screen.getByTestId('calc');
    const equal_button = screen.getByTestId(`${base_test_id}_=`);
    const display = screen.getByTestId(display_id);

    // Use https://keycode.info to find keypress info
    fireEvent.keyPress(calculator, {key: '1', code: 'Digit1', charCode: 49});
    fireEvent.keyPress(calculator, {key: '*', code: 'NumpadMultiply', charCode: 106});
    fireEvent.keyPress(calculator, {key: '2', code: 'Digit2', charCode: 50});
    act(() => equal_button.click());
    fireEvent.keyPress(calculator, {key: '*', code: 'Digit8', shiftKey: true, charCode: 56});
    fireEvent.keyPress(calculator, {key: '3', code: 'Digit3', charCode: 51});
    act(() => equal_button.click());
    fireEvent.keyPress(calculator, {key: 'x', code: 'KeyX', charCode: 88});
    fireEvent.keyPress(calculator, {key: '3', code: 'Digit3', charCode: 51});
    act(() => equal_button.click());

    expect(display.textContent).toEqual('18');
  });

  it('test "AC" button', () => {
    render(<Calculator/>);

    const one_button = screen.getByTestId(`${base_test_id}_1`);
    const ac_button = screen.getByTestId(`${base_test_id}_AC`);
    const display = screen.getByTestId(display_id);

    expect(ac_button.textContent).toEqual('AC');

    act(() => one_button.click());
    expect(display.textContent).toEqual('1');

    act(() => ac_button.click());
    expect(display.textContent).toEqual('0');
  });

  it('test "AC" turns to "C"', () => {
    render(<Calculator/>);

    const one_button = screen.getByTestId(`${base_test_id}_1`);
    const ac_button = screen.getByTestId(`${base_test_id}_AC`);

    act(() => one_button.click());
    expect(ac_button.textContent).toEqual('C');
  });

  it('test "AC" turns back to "AC" if pressed once', () => {
    render(<Calculator/>);

    const one_button = screen.getByTestId(`${base_test_id}_1`);
    const ac_button = screen.getByTestId(`${base_test_id}_AC`);

    act(() => one_button.click());
    act(() => ac_button.click());
    expect(ac_button.textContent).toEqual('AC');
  });

  it('test "AC" clears operations if pressed twice', () => {
    render(<Calculator/>);

    const one_button = screen.getByTestId(`${base_test_id}_1`);
    const two_button = screen.getByTestId(`${base_test_id}_2`);
    const plus_button = screen.getByTestId(`${base_test_id}_+`);
    const equal_button = screen.getByTestId(`${base_test_id}_=`);
    const ac_button = screen.getByTestId(`${base_test_id}_AC`);
    const display = screen.getByTestId(display_id);

    // Type "1" and then press AC/C twice
    act(() => one_button.click());
    act(() => ac_button.click());
    act(() => ac_button.click());
    // Type 2 + 2 to see if operations really cleared.
    act(() => ac_button.click());
    act(() => two_button.click());
    act(() => plus_button.click());
    act(() => two_button.click());
    act(() => equal_button.click());
    expect(display.textContent).toEqual('4');
  });
});

describe('Calculator - Operation Tests', () => {
  const buttonList = [ '+', '-', 'x', '÷' ];

  buttonList.forEach((btn) => {
    it(`test "${btn}" button does not go to display`, () => {
      render(<Calculator/>);

      const one_button = screen.getByTestId(`${base_test_id}_1`);
      const two_button = screen.getByTestId(`${base_test_id}_2`);
      const three_button = screen.getByTestId(`${base_test_id}_3`);
      const op_button = screen.getByTestId(`${base_test_id}_${btn}`);
      const display = screen.getByTestId(display_id);

      act(() => {
        one_button.click();
        two_button.click();
        three_button.click();
        op_button.click();
      });

      expect(display.textContent).toEqual('123');
    });
  });

  it('test "=" calculates operations', () => {
    render(<Calculator/>);

    const one_button = screen.getByTestId(`${base_test_id}_1`);
    const plus_button = screen.getByTestId(`${base_test_id}_+`);
    const equal_button = screen.getByTestId(`${base_test_id}_=`);
    const display = screen.getByTestId(display_id);

    act(() => {
      one_button.click();
      plus_button.click();
      one_button.click();
      equal_button.click();
    });

    expect(display.textContent).toEqual('2');
  });

  it('test multiple "+" operations output to display correctly', () => {
    render(<Calculator/>);

    const one_button = screen.getByTestId(`${base_test_id}_1`);
    const plus_button = screen.getByTestId(`${base_test_id}_+`);
    const equal_button = screen.getByTestId(`${base_test_id}_=`);
    const display = screen.getByTestId(display_id);

    act(() => {
      one_button.click();
      one_button.click();
      plus_button.click();
      one_button.click();
      one_button.click();
      equal_button.click();
      plus_button.click();
      one_button.click();
      one_button.click();
    });

    expect(display.textContent).toEqual('11');
  });

  it('test "." not truncated on display', () => {
    render(<Calculator/>);

    const one_button = screen.getByTestId(`${base_test_id}_1`);
    const dot_button = screen.getByTestId(`${base_test_id}_.`);
    const display = screen.getByTestId(display_id);

    act(() => {
      one_button.click();
      dot_button.click();
    });

    expect(display.textContent).toEqual('1.');
  });
});
