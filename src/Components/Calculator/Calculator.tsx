import React from "react";
import CalcButton from "../CalcButton/CalcButton";
import TopLeftOperator from "../CalcButton/TopLeftOperator";
import MainOperator from "../CalcButton/MainOperator";
import Display from "../Display";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import CalculatorState from "./CalculatorState";
import * as MathJS from "mathjs";
import './Calculator.css'

const operators = [ '+', '-', 'x', '÷', /*'=',*/ '±', '%', '/', '*' ];
const allowedKeys = [...operators, '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '\\', '=', 'x', 'Equal', 'Enter'];

export function isAllowedKey(key : string) : boolean {
  return allowedKeys.includes(key)
}

class Calculator extends React.Component<any, CalculatorState> {
  private readonly ref : React.RefObject<HTMLDivElement>

  constructor(props : any) {
    super(props);

    // This allows us to focus the main div
    // immediately to allow key presses to work.
    this.ref = React.createRef();

    this.state = {
      display_string: '0',
      operations: [],
      exp_invalid: false,
    };
  }

  getUpdatedDisplayString = (updatedArray : Array<string | number>, item : (string | number)) : string => {
    if (item === '=' || (typeof item === 'string' && operators.includes(item))) {
      const mathExp = updatedArray.slice(0, updatedArray.length - 1)
          .join("")
          .replace('=', '');
      return MathJS.evaluate(mathExp).toString();
    } else if (updatedArray.length) {
      const lastOperandIndex = updatedArray.slice().reverse()
          .findIndex( (ele) => typeof ele === 'string' && operators.includes(ele));
      if (lastOperandIndex > -1) {
        // Display everything after the last operand
        const minusAmount = (updatedArray.length === lastOperandIndex) ?
            lastOperandIndex + 1 :
            lastOperandIndex;

        return updatedArray.slice(updatedArray.length - minusAmount).join("");
      }
    }

    return MathJS.evaluate(updatedArray.join("")).toString();
  }

  addToOperations = (item : (string | number)) => {
    this.setState((prevState : CalculatorState) => {
      if (item === '÷') {
        item = '/';
      } else if (item === 'x') {
        item = '*';
      }

      let updatedArray = [...prevState.operations, item];
      let display_string : string;
      let invalid_exp = false;

      try {
        display_string = this.getUpdatedDisplayString(updatedArray.slice(0), item);
      } catch (e) {
        display_string = "INVALID";
        invalid_exp = true
      }

      if (item === '.' && !display_string.endsWith('.')) {
        display_string += '.';
      } else if (item === '=') {
        updatedArray = ['=', Number(display_string)];
      }

      return {
        ...prevState,
        display_string: display_string,
        operations: updatedArray,
        exp_invalid: invalid_exp,
      };
    });
  }

  clearButtonLabel = () => {
    if (!this.state.exp_invalid && this.state.operations.length && this.state.display_string !== '0') {
      return 'C';
    }

    return 'AC';
  }

  clearOperationsAndDisplay = () => {
    let shouldResetCalcState = true;

    if ( !this.state.exp_invalid && this.clearButtonLabel() === 'C') {
      const lastOperandIndex = this.state.operations.slice().reverse()
          .findIndex( (ele) => typeof ele === 'string' && operators.includes(ele));
      if (lastOperandIndex > -1) {
        shouldResetCalcState = false;

        // Delete everything after the last operand
        const lastIndex = this.state.operations.length - lastOperandIndex;

        this.setState((prevState : CalculatorState) => {
          return {
            ...prevState,
            display_string: '0',
            operations: this.state.operations.slice(0, lastIndex)
          };
        });
      } else if (this.state.operations[0] === '=') {
        // Since this "=" is the only operation sign,
        // leave operations alone and ONLY clear the display.
        shouldResetCalcState = false;

        this.setState((prevState : CalculatorState) => {
          return {
            ...prevState,
            display_string: '0',
          };
        });
      } else {
        // We haven't done an operation yet so just treat the "C" as a "AC"
        shouldResetCalcState = true;
      }
    }

    if (shouldResetCalcState) {
      this.setState((prevState : CalculatorState) => {
        return {
          ...prevState,
          display_string: '0',
          operations: [],
          exp_invalid: false,
        };
      });
    }
  }

  handleKeyPress = (e : React.KeyboardEvent) => {
    let opToAdd = e.key;

    if ( !this.state.exp_invalid && isAllowedKey(e.key)) {
      switch (e.key) {
        case 'Enter':
          opToAdd = '=';
          break;
        case '*':
          opToAdd = 'x';
          break;
        case '/':
        case '\\':
          opToAdd = '÷';
          break;
      }

      this.addToOperations(opToAdd);
    }
  }

  componentDidMount() {
    this.ref.current?.focus();
  }

  render() {
    return (
        <Container
            data-testid="calc"
            fluid="md"
            ref={this.ref}
            tabIndex={-1}
            className="calc"
            onKeyPress={this.handleKeyPress}
        >
          <Row className="justify-content-md-center">
            <Col xs="auto" sm={6} md={4}>
              <Row>
                <Display display_string={this.state.display_string} />
              </Row>

              <Row>
                <TopLeftOperator button_label={this.clearButtonLabel()} on_click={this.clearOperationsAndDisplay}/>
                <TopLeftOperator button_label="±" on_click={this.addToOperations}/>
                <TopLeftOperator button_label="%" on_click={this.addToOperations}/>
                <MainOperator button_label="÷" on_click={this.addToOperations}/>
              </Row>

              <Row>
                <CalcButton button_label="7" on_click={this.addToOperations}/>
                <CalcButton button_label="8" on_click={this.addToOperations}/>
                <CalcButton button_label="9" on_click={this.addToOperations}/>
                <MainOperator button_label="x" on_click={this.addToOperations}/>
              </Row>

              <Row>
                <CalcButton button_label="4" on_click={this.addToOperations}/>
                <CalcButton button_label="5" on_click={this.addToOperations}/>
                <CalcButton button_label="6" on_click={this.addToOperations}/>
                <MainOperator button_label="-" on_click={this.addToOperations}/>
              </Row>

              <Row>
                <CalcButton button_label="1" on_click={this.addToOperations}/>
                <CalcButton button_label="2" on_click={this.addToOperations}/>
                <CalcButton button_label="3" on_click={this.addToOperations}/>
                <MainOperator button_label="+" on_click={this.addToOperations}/>
              </Row>

              <Row>
                <CalcButton button_label="0" on_click={this.addToOperations} col_size={6}/>
                <CalcButton button_label="." on_click={this.addToOperations}/>
                <MainOperator button_label="=" on_click={this.addToOperations}/>
              </Row>

            </Col>
          </Row>
        </Container>
    );
  }
}

export default Calculator;