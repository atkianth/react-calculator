export default interface CalculatorState {
  display_string : string,
  operations : Array<string | number>,
  exp_invalid : boolean,
}