import React from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import CalcButtonProps from "./CalcButtonProps";
import "./CalcButton.css"

interface CalcButtonState {
  hover : boolean
}

export function shouldHandleEvent(e : React.SyntheticEvent) {
  if (typeof PointerEvent !== 'function') {
    // This is to allow jsdom to work still as it doesn't have
    // PointerEvent defined. See https://github.com/jsdom/jsdom/pull/2666
    return true;
  }

  return e.nativeEvent instanceof PointerEvent && e.nativeEvent.detail > 0;
}

class CalcButton extends React.Component<CalcButtonProps, CalcButtonState> {
  constructor(props : CalcButtonProps) {
    super(props);

    this.state = {
      hover: false
    };
  }

  createBtnStyle = () => {
    let bgColor : string;

    if (this.state.hover) {
      bgColor = this.props.hover_bg_color || '#d7d8d7'
    } else {
      bgColor = this.props.bg_color || '#7A7C7B'
    }

    return {
      backgroundColor: bgColor
    }
  }

  setHoverFalse = () => {
    this.setState({ hover: false });
  }

  setHoverTrue = () => {
    this.setState({ hover: true });
  }

  onClick = (e : React.SyntheticEvent) => {
    if (shouldHandleEvent(e)) {
      this.props.on_click(this.props.button_label);
    }
  }

  render() {
    return (
        <Col md={this.props.col_size || 3} className="p-0 m-0">
          <Button
              variant="primary"
              data-testid={`calc_btn_${this.props.button_label}`}
              onClick={this.onClick}
              size="lg"
              style={this.createBtnStyle()}
              className="calc-btn"
              onMouseEnter={this.setHoverTrue}
              onMouseLeave={this.setHoverFalse}
          >
            {this.props.button_label}
          </Button>
        </Col>
    );
  }
}

export default CalcButton;
