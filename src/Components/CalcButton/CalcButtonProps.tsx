export default interface CalcButtonProps {
  button_label : String,
  on_click : Function,
  col_size ?: number,
  bg_color ?: string,
  hover_bg_color ?: string,
}