import React from "react";
import CalcButton from "./CalcButton"
import CalcButtonProps from "./CalcButtonProps";

export default class MainOperator extends React.Component<CalcButtonProps, any> {
  render() {
    return (
        <CalcButton
            {...this.props}
            bg_color='#F29D28'
            hover_bg_color='#fbe2bf'
        />
    )
  }
}