import CalcButton from "./CalcButton"
import CalcButtonProps from "./CalcButtonProps";
import React from "react";

export default class TopLeftOperator extends React.Component<CalcButtonProps, any> {
  render() {
    return (
        <CalcButton
            {...this.props}
            bg_color='#606261'
            hover_bg_color='#cfd0d0'
        />
    )
  }
}