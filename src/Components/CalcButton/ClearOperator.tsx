import CalcButton, {shouldHandleEvent} from "./CalcButton"
import CalcButtonProps from "./CalcButtonProps";
import React from "react";

export default class ClearOperator extends React.Component<CalcButtonProps, any> {

  onClick = (e : React.SyntheticEvent) => {
    if (shouldHandleEvent(e)) {
      this.props.on_click(e);
    }
  }

  render() {
    return (
        <CalcButton
            {...this.props}
            bg_color='#606261'
            hover_bg_color='#cfd0d0'
            on_click={this.onClick}
        />
    )
  }
}