import React, {CSSProperties} from "react";

interface DisplayProps {
  display_string : String
}

const displayStyle : CSSProperties = {
  backgroundColor: '#4F5150',
  color: '#FFFFFF',
  textAlign: 'right',
  fontWeight: "normal",
  fontSize: "3em",
}

function Display(props : DisplayProps) {
  return (
      <div
          data-testid="calc_display"
          style={displayStyle}
      >
        {props.display_string || '0'}
      </div>
  );
}

export default Display;