import React from 'react';
import Container from 'react-bootstrap/Container';
import './App.css';
import Calculator from "./Components/Calculator/Calculator";

function App() {
  return (
    <Container className="p-3 App">
      <Container className="p-5 mb-4 rounded-3">
        <h1 className="header">Calculator</h1>
      </Container>

      <Container className="">

        <Calculator />
      </Container>


    </Container>
  );
}

export default App;
